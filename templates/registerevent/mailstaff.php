<p>Hi,<br /> 
there is a new event registered on https://fsfe.org/events/tools/eventregistration</p>

<p>Below is a list of the information that were provided.</p>
<ul>
    <li>Name: <?=$name?></li>
    <li>Email: <?=$email?></li>
    <li>Event Title: <?=$title?></li>
    <li>Location: <?=$location?></li>
</ul>
 
<p>Find attached the event data as XML file for the webpage and data file for the Wiki. Please upload the
XML file in the next 24 hours to the webpage. Or contact the contributor for clarifications.</p>
 
<p>Thanks,<br />
your website</p>